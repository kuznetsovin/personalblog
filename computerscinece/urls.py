# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from blog.views import *
from users.views import *
from django.conf import settings
from blog.feeds import RSSFeed

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'computerscinece.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^$', index),
    url(r'^categories/(?P<category_id>\d+)', index),
    url(r'^tags/(?P<tag_id>\d+)', index),
    url(r'^diggest/', include('digest.urls')),
    url(r'^login', user_login),
    url(r'^logout', user_logout),
    #url(r'^registration', registration),
    url(r'^add_post', add_post),
    url(r'^searchtags', search_tags),
    url(r'^posts/(?P<id_post>\d+)', post, name='posts'),
    url(r'^addcomment', add_comment),
    url(r'^voiting', voiting),
    url(r'^favorites', favorites_post),  
    url(r'^markitup/', include('markitup.urls')),
    url(r'^uploadattach', image_upload),
    url(r'^upload/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^howto', howto_page),
    url(r'^datasets', dataset_page),
    url(r'^helplink', helplink_page),
    url(r'^rss/$', RSSFeed()),
    url(r'^profile/(?P<usrname>\w+)/$', view_profile),
    url(r'^profile/edit$', edit_profile),
    url(r'^searchskills$', find_skills),
    url(r'^userrate$', user_ranking),
    #old 301 redirect page
    url(r'^комлиция-boost-python-и-привязка-к-eclipse/$'.decode('utf-8'), RedirectView.as_view(url='/posts/1', permanent=True)),
    url(r'^парсинг-fb2-с-помощью-lxml/$'.decode('utf-8'), RedirectView.as_view(url='/posts/3', permanent=True)),
    url(r'^28/$', RedirectView.as_view(url='/posts/8', permanent=True)),
    url(r'^онлайн-конференция-по-программным-пр/$'.decode('utf-8'), RedirectView.as_view(url='/posts/9', permanent=True)),
    url(r'^hodan-ищет-то-чего-не-ищет-google/$'.decode('utf-8'), RedirectView.as_view(url='/posts/10', permanent=True)),
    url(r'^склейка-в-строку-нескольких-значений/$'.decode('utf-8'), RedirectView.as_view(url='/posts/11', permanent=True)),
    url(r'^удаление-строк-из-внешней-таблицы-ms-access-2010-ч/$'.decode('utf-8'), RedirectView.as_view(url='/posts/12', permanent=True)),
    url(r'^отправка-писем-из-vba-через-lotus-notes/$'.decode('utf-8'), RedirectView.as_view(url='/posts/13', permanent=True)),
    url(r'^update-c-несколькими-таблицами-oracle/$'.decode('utf-8'), RedirectView.as_view(url='/posts/15', permanent=True)),
    url(r'^конференция-по-web-аналитике-imetrics/$'.decode('utf-8'), RedirectView.as_view(url='/posts/16', permanent=True)),
    url(r'^576k-экспериментов-и-130-наборов-данных-для-м/$'.decode('utf-8'), RedirectView.as_view(url='/posts/18', permanent=True)),
    url(r'^исправление-ошибки-при-компиляции-pyinstaller/$'.decode('utf-8'), RedirectView.as_view(url='/posts/20', permanent=True)),
    url(r'^yet-another-conference-2013/$'.decode('utf-8'), RedirectView.as_view(url='/posts/21', permanent=True)),
    url(r'^вышла-новая-версия-пакета-rpy/$'.decode('utf-8'), RedirectView.as_view(url='/posts/24', permanent=True)),
    url(r'^вышла-новая-версия-sklearn-pandas/$'.decode('utf-8'), RedirectView.as_view(url='/posts/26', permanent=True)),
    url(r'^sql-saturday-в-москве/$'.decode('utf-8'), RedirectView.as_view(url='/posts/27', permanent=True)),
    url(r'^анализ-сложности-алгоритмов-для-начи/$'.decode('utf-8'), RedirectView.as_view(url='/posts/28', permanent=True)),
    url(r'^7-шагов-для-развития-в-области-аналитик/$'.decode('utf-8'), RedirectView.as_view(url='/posts/29', permanent=True)),
    url(r'^519/$', RedirectView.as_view(url='/posts/30'.decode('utf-8'), permanent=True)),
    url(r'^выбор-модели-arma-по-акф-и-чакф/$'.decode('utf-8'), RedirectView.as_view(url='/posts/31', permanent=True)),
    url(r'^основные-этапы-анализ-временных-рядо/$'.decode('utf-8'), RedirectView.as_view(url='/posts/33', permanent=True)),
    url(r'^этапы-построения-регрессионной-моде/$'.decode('utf-8'), RedirectView.as_view(url='/posts/34', permanent=True)),
    url(r'^новый-цифровой-мир-эрика-шмидта/$'.decode('utf-8'), RedirectView.as_view(url='/posts/47', permanent=True)),
    url(r'^рейтинг-языков-программирования-для-data/$'.decode('utf-8'), RedirectView.as_view(url='/posts/48', permanent=True)),
    url(r'^55/$', RedirectView.as_view(url='/posts/49'.decode('utf-8'), permanent=True)),
    url(r'^обработка-смысловой-информации-или-ч/$'.decode('utf-8'), RedirectView.as_view(url='/posts/50', permanent=True)),
    url(r'^bigml-машинное-обучение-это-просто/$'.decode('utf-8'), RedirectView.as_view(url='/posts/51', permanent=True)),
    url(r'^dbforge-studio-for-oracle-удобная-среда-для-работы-с-бд/$'.decode('utf-8'), RedirectView.as_view(url='/posts/52', permanent=True)),
    url(r'^введение-в-pandas-или-анализ-данных-на-python/$'.decode('utf-8'), RedirectView.as_view(url='/posts/53', permanent=True)),
    url(r'^ведение-в-визуализацию-данных-при-ан/$'.decode('utf-8'), RedirectView.as_view(url='/posts/54', permanent=True)),
    url(r'^строим-простую-картограмму-pandasvincent/$'.decode('utf-8'), RedirectView.as_view(url='/posts/55', permanent=True)),
    url(r'^введение-в-анализ-данных-с-помощью-pandas-и-scikit/$'.decode('utf-8'), RedirectView.as_view(url='/posts/56', permanent=True)),
    url(r'^отчет-о-решении-задачи-кредитного-ско/$'.decode('utf-8'), RedirectView.as_view(url='/posts/57', permanent=True)),
    url(r'^введение-в-анализ-текстовой-информац/$'.decode('utf-8'), RedirectView.as_view(url='/posts/58', permanent=True)),
    url(r'^пример-решения-задачи-множественной/$'.decode('utf-8'), RedirectView.as_view(url='/posts/59', permanent=True)),
    url(r'^анализ-временных-рядов-с-помощью-statsmodelspython/$'.decode('utf-8'), RedirectView.as_view(url='/posts/60', permanent=True)),
    url(r'^построение-модели-sarima-с-помощью-pythonr/$'.decode('utf-8'), RedirectView.as_view(url='/posts/61', permanent=True)),
    
)

#SOCIAL_AUTH_PIPELINE = (
#    'social_auth.backends.pipeline.social.social_auth_user',
#    'social_auth.backends.pipeline.associate.associate_by_email',
#    'social_auth.backends.pipeline.user.get_username',
#    'social_auth.backends.pipeline.user.create_user',
#    'social_auth.backends.pipeline.social.associate_user',
#    'social_auth.backends.pipeline.social.load_extra_data',
#    'social_auth.backends.pipeline.user.update_user_details'
#)