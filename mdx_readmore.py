﻿from __future__ import absolute_import
from __future__ import unicode_literals
from markdown import Extension
from markdown.postprocessors import Postprocessor
import re

MORE_RE = re.compile(r'<readmore>')
 
class ReadMoreExtension (Extension):
    def extendMarkdown(self, md, md_globals):
        md.postprocessors.add("readmore", ReadMorePostprocessor(md), "_end")

        
class ReadMorePostprocessor(Postprocessor):
    def run(self, text):    
        text = MORE_RE.sub('<a name="readmore"></a>',text)
        return text        

        
def makeExtension(configs={}):
    return ReadMoreExtension(configs=configs)