function send_comment(btn, idpost){
	var commentid = $(btn).parent().children("#commentid").val();
	var text = $("textarea[name='text']").val();
    var key = $("input[name='csrfmiddlewaretoken']").val();
    if (text == '')
        alert("не введен комментарий")
    else
        $.post('/addcomment',
            {
            post: idpost,
            comment: text,
            parentid: commentid,
            csrfmiddlewaretoken: key
            },
            function (data) {
                $("textarea[name='text']").val('');          
                $('#add_comment').appendTo('.comment_div').addClass('hidden');
                $('#add_comment > #commentid').remove();
                $('.comment').remove();          
                $.each(data, function(index, comment){
                        $('#print_comment_block').append(print_comment(idpost, comment))
                        }               
                    )
                $('.comment').on("click", "a.reply", function(e){
                                                        e.preventDefault();
                                                        add_new_comment(this, idpost);
                                                        });
                $('.comment').on("click", "a.plus", function(e){
                                                        e.preventDefault();
                                                        voicing(this, 1);
                                                        });
                $('.comment').on("click", "a.minus", function(e){
                                                        e.preventDefault();
                                                         voicing(this, -1);
                                                        });
                $('.comment_div > a').removeClass('hidden');
                },
            'json')
	return false
	} 
    
function add_new_comment(href, postid){
    var comment = $(href).parent().attr("id");
    if (comment) {
		$frm = $('#add_comment').removeClass('hidden');
		$('<input/>').appendTo($frm).attr({'type':'hidden','id':'commentid'}).val(comment); 
        $frm.appendTo($('#' + comment + '> .reply_form'));
        $('#' + comment + '> a.reply').addClass('hidden');
		} 
	else {
		$('#add_comment').removeClass('hidden');
        $('.comment_div > a').addClass('hidden');
		}
         
    return false;
	}
   
function print_comment(post, rqst){
    $cmt = $('<div/>').addClass('comment').css('margin-left',rqst.level*10).attr('id', rqst.id)
	$('<input/>').appendTo($cmt).attr({'type':'hidden','id':'commentid'}).val(rqst.id);    
    $('<div/>').appendTo($cmt).addClass('username').text(rqst.username);
    $('<div/>').appendTo($cmt).addClass('comment_text').html(rqst.text);			
    $vocing = $('<div/>').attr('id','voicing');    
    $a = $('<a/>').addClass('plus').attr({'href':'#', 'title':'Нравится'});
    $vocing.append($a);
    $rating = $('<div/>').addClass('rating').attr('id','comment_'+rqst.id).text(rqst.rating);
    if (parseInt(rqst.rating) > 0)  $rating.addClass('positive');
    if (parseInt(rqst.rating) < 0)  $rating.addClass('negative');       
    $rating.appendTo($vocing)
    $a = $('<a/>').addClass('minus').attr({'href':'#', 'title':'Не нравится'});
    $vocing.append($a);
    $cmt.append($vocing).append('&#149;');
    $a = $('<a/>').addClass('reply').text('Ответить'); 
    $cmt.append($a)
    $('<div/>').appendTo($cmt).addClass('reply_form')
    return $cmt
    }