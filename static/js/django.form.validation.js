$(document).ready(function(){   
    $('#addform').submit(function(){
        $form = $('#addform')
        $modal_win = $("#modal_win")        
        $.post($form.attr('action'),
                $form.serialize(),
                function(data){
                    if (data.error)
                        $.each(data.error, function(index, err){
                                $error_tag = $('<ul/>').addClass("errorlist")
                                $('<li/>').appendTo($error_tag).text(err.error)
                                $("#id_" + err.id).after($error_tag)
                                }
                            )
                    else {
                        $modal_win.modal('hide')
                        $form[0].reset()
                        }
                    },
                'json')
        return false;
        });
    });  