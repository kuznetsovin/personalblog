$(document).ready(function() {
    $("#id_tags").tokenInput("/searchtags", {
                theme: "facebook",
				hintText: "Поиск тегов",
				noResultsText: "Нет найденых тегов",
				searchingText: "Поиск...",
				tokenDelimiter: ",",
				allowFreeTagging: true
            });   
});