/*$(document).ready(function () {
	$("#fav > a").click(favorites_post(this));	
  });*/
  
function update_rating(rating, elid){
    obj = $('#' + elid)
    obj.text(rating);
	if (rating > 0) 
        obj.css('color', '#01DF01')			
	else
        if (rating < 0) 
            obj.css('color', '#FF0000')
		else
            obj.css('color', '#000')
    }
  
function voicing(href, voice){
    elid = $(href).parent().children(".rating").attr("id")
    if (~elid.indexOf('comment')){
        comment_id = elid.slice(8)
        post_id = null
        }
    else {
        comment_id = null
        post_id = elid.slice(5)
        }
	$.post("/voiting", 
			{
			post: post_id,
			voice: voice,
			comment: comment_id
	        },
	        function(data) {
                if (!(data.reply_voiting)){
                    update_rating(data.new_rating, elid)
                    $.jGrowl("Ваш голос учтен.", {theme:"voicing", life: 1000 });
                    }
                else
                    $.jGrowl("Вы уже голосованили.", {theme:"err_voice", life: 1000 });
	        	},
	        'json'
	    );
	    return false;
	};

function favorites_post (obj, post_id){
	$.post("/favorites", 
		{post: post_id},
		 function(data) {
            $(obj).toggleClass('favorite_add')
			$(obj).toggleClass('favorite_del')
            if (data.operation == 1)
                $.jGrowl("Пост добавлен в избранное.", {theme:"add_fav", life: 1000 });							
			else 
				$.jGrowl("Пост удален из избранного.", {theme:"del_fav", life: 1000 });
		 	},
		'json');
	return false;
	}
 
 function err_voice () {
    $.jGrowl("Голосовать могут только зарегестрированные пользователи", {theme:"err_voice", life: 1000 });
    return false;
    }
 
  function err_fav () {
    $.jGrowl("Вы не зарегестрированы", {theme:"err_voice", life: 1000 });
    return false;
    }