from django.contrib import admin
from users.models import *
# Register your models here.


admin.site.register(City)
admin.site.register(Country)
admin.site.register(Profile)
admin.site.register(SocialNet)
admin.site.register(Skills)