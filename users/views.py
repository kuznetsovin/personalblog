from django.shortcuts import render, redirect, render_to_response, get_object_or_404, HttpResponse
from django.db.models import Count, Sum
from django.views.decorators.csrf import csrf_exempt 
from django import http
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from users.forms import *
from users.models import Profile, SocialNet
from blog.models import Post
from ast import literal_eval
from functions import *
import json

def user_login(request):
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
        return redirect('/')
    return render_to_response('login.html', {'form': LoginForm,}, context_instance=RequestContext(request))

def user_logout(request):
    logout(request)
    return redirect(request.META['HTTP_REFERER'])
    
def registration(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            User.objects.create_user(username, email, password)
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/')
    else:
        form = RegisterForm()
    return render_to_response('register.html', {'form': form,},  context_instance=RequestContext(request))

def view_profile(request, usrname):
    user_info = get_object_or_404(Profile, user__username=usrname)
    social_account_list = literal_eval(user_info.social)  
    social = []
    list_favorites = []
    for acc in social_account_list.keys():
        social_link = SocialNet.objects.get(network_name=acc).link
        social.append({'name': acc, 'link' : social_link % social_account_list[acc], 'account': social_account_list[acc]})
    if user_info.favorit_post:        
        favorites = user_info.favorit_post[1:].split(',')
        for post in favorites:
            list_favorites.append(Post.objects.get(id = int(post)))
    
    color ={}
    if user_info.rating > 0:
        color['rating'] = 'positive'
    elif user_info.rating < 0:
        color['rating'] = 'negative'
    else:
        color['rating'] = ''
    
    if user_info.karma > 0:
        color['karma'] = 'positive'
    elif user_info.rating < 0:
        color['karma'] = 'negative'
    else:
        color['karma'] = ''
        
    return render_to_response('profile.html', {'profile': user_info, 'social_account': social,'favorites': list_favorites, 'color':color},  context_instance=RequestContext(request))

def edit_profile(request):
    usr = get_object_or_404(Profile, pk=request.user)
    soc = SocialNet.objects.all()
    parse_social = literal_eval(usr.social)
    contacts = [{'name':i, 'prof': parse_social[i]} for i in parse_social.keys()]
    
    if request.method == 'POST':        
        form = EditProfileForm(data=request.POST, files=request.FILES)
        update_fields = ['birthday','country','city','phone','about']
        
        if form.is_valid():
            for param in update_fields:
                if form.cleaned_data[param] <> getattr(usr, param):
                    setattr(usr, param, form.cleaned_data[param])                 
            
            new_contacts = {}
            for social in soc:                                   
                if request.POST.get(social.network_name): 
                    new_contacts[social.network_name] = request.POST[social.network_name]
            
            usr.social = str(new_contacts)            
            
            if request.FILES.get('avatar'):
                import os
                from django.conf import settings
                fp = os.path.join(settings.MEDIA_ROOT,str(usr.avatar.name))
                if os.path.exists(fp):
                    os.remove(fp)
                usr.avatar = request.FILES.get('avatar')
            
            usr.save()
            
            for new_skill in form.cleaned_data['skills'].split(','):                    
                usr.skills.add(
                    token_validate(new_skill, Skills)
                    )
                    
            return redirect('/profile/%s' % request.user.username) 
    else:
        acc_skills = json.dumps([{"id" : skill.id, "name" : skill.name} for skill in usr.skills.all()])        
        form = EditProfileForm(instance=usr)
    return render_to_response('edit_profile.html', {'form': form, 'skills': acc_skills, 'social': soc, 'contacts': contacts},  context_instance=RequestContext(request))

def find_skills(request):
    return http.HttpResponse(token_search(request, Skills), content_type="application/json")
    
@csrf_exempt
def user_ranking(request):
    if request.is_ajax():        
        usr = int(request.POST['usr'])
        usr_info = Profile.objects.annotate(sum_post_rate = Sum('post__rating'), count_post = Count('post__id')).get(user__id = usr)
        voice = int(request.POST['voice'])
        
        try:
            RakingUsersRate.objects.get(user_voicing=request.user, raking_user=usr_info)          
            reply_voicing = True
        except RakingUsersRate.DoesNotExist:               
            import math

            usr_info.karma += voice

            sum_post_rate = float(usr_info.sum_post_rate)
            karma = float(usr_info.karma)
            count_post = float(usr_info.count_post)
            usr_info.rating = int(math.ceil((sum_post_rate*karma/count_post)))
            usr_info.save()
            
            RakingUsersRate.objects.create(user_voicing=request.user.get_profile(), raking_user=usr_info, voice=voice)
            reply_voicing = False            
        
        rating = {'karma': usr_info.karma, 'rating': usr_info.rating, 'reply_voiting': reply_voicing}
        return HttpResponse(json.dumps(rating)) 