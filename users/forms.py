# -*- coding: utf-8 -*-

from django import forms
from django.forms.extras.widgets import SelectDateWidget
from users.models import *
from django.contrib.auth.models import User
from datetime import datetime
from widgets import ImageWidget


class RegisterForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'Имя пользователя')  
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','id':'inputPassword'}), label=u'Пароль') 
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'E-mail')  
    class Meta:
        model = User
        fields = ('username','password','email')
        
class LoginForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'Имя пользователя', help_text="")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label=u'Пароль')
    class Meta:
        model = User
        fields = ('username','password')

class EditProfileForm(forms.ModelForm):
    avatar = forms.ImageField(widget=ImageWidget, label=u'Аватар', required=False)
    birthday = forms.DateField(widget=SelectDateWidget(years=range(1950, 2008)), label=u"Дата рождения", required=False)
    country = forms.ModelChoiceField(queryset=Country.objects.all().order_by('name'), widget=forms.Select(attrs={'class': 'form-control'}), label=u'Страна', required=False)    
    city = forms.ModelChoiceField(queryset=City.objects.all().order_by('name'), widget=forms.Select(attrs={'class': 'form-control'}), label=u'Город', required=False)
    phone = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'Телефон', required=False)
    about = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label=u'О себе', required=False)
    skills = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Навыки", required=False)      
    
    class Meta:
        model = Profile
        fields = ('avatar','birthday','country','city','phone','about','skills')