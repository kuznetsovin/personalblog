from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save 
import os
        
# Create your models here.


class City(models.Model):
    name = models.TextField(max_length=200)

    def __unicode__(self):
        return self.name


class Country(models.Model):
    name = models.TextField(max_length=200)

    def __unicode__(self):
        return self.name

class SocialNet(models.Model):
    network_name = models.CharField(max_length=150)
    link = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.network_name

class Skills(models.Model):
    name = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.name
        
class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True, unique=True)
    avatar = models.ImageField(upload_to="avatars", null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    karma = models.IntegerField(max_length=4, default=0)
    rating = models.IntegerField(max_length=5, default=0)
    about = models.TextField(max_length=1000, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)
    social = models.CharField(max_length=300, null=True, blank=True)
    favorit_post = models.CharField(max_length=1000, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    skills = models.ManyToManyField(Skills, null=True, blank=True)
    
    def __unicode__(self):
        return self.user.username           
    
    def create_avatar(self):
 
        from PIL import Image
        from cStringIO import StringIO
        from django.core.files.uploadedfile import SimpleUploadedFile
         
        AVATAR_SIZE = (200, 200)

        DJANGO_TYPE = self.avatar.file.content_type
 
        if DJANGO_TYPE == 'image/jpeg':
             PIL_TYPE = 'jpeg'
             FILE_EXTENSION = 'jpg'
        elif DJANGO_TYPE == 'image/png':
             PIL_TYPE = 'png'
             FILE_EXTENSION = 'png'
 
        image = Image.open(StringIO(self.avatar.read()))
        usrnm = self.user.username        
        
        image.thumbnail(AVATAR_SIZE, Image.ANTIALIAS)
            
        temp_handle = StringIO()
        image.save(temp_handle, PIL_TYPE)
        temp_handle.seek(0)
        
        suf = SimpleUploadedFile(usrnm, temp_handle.read(), content_type=DJANGO_TYPE)
        
        self.avatar.save('%s.%s'% (usrnm, FILE_EXTENSION), suf, save=False)
                
    def save(self, *args, **kwargs):
        if self.avatar:            
            if 'avatars/' not in self.avatar.name:
                self.create_avatar()
        super(Profile, self).save()

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)

class RakingUsersRate(models.Model):    
    user_voicing = models.ForeignKey(Profile, related_name='ursvoicing')
    raking_user = models.ForeignKey(Profile, related_name='usrrate')     
    voice = models.IntegerField(max_length=2)    