﻿from django.contrib import admin
from digest.models import *

admin.site.register(DiggetCategoryNews)
admin.site.register(DiggestInfo)
admin.site.register(DiggestNews)

