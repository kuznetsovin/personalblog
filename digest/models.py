﻿from django.db import models

CHOISE_LANG = (
    ('EN', 'Английский'),
    ('RU', 'Русский'),
    )

class DiggetCategoryNews(models.Model):
    name = models.CharField(max_length=150)
    
    def __unicode__(self):
        return self.name

class DiggestInfo(models.Model):
    first_date = models.DateField()
    last_date = models.DateField()
    href = models.CharField(max_length=150,  blank=True, null=True)
    archive = models.BooleanField(default=False)
    
    def get_diggest_period(self):
        from datetime import timedelta
        
        numdays = (self.last_date - self.first_date).days
        return [self.first_date + timedelta(days=x) for x in range(0, numdays)]
    
    def __unicode__(self):
        return u'Diggest №%s (%s - %s)' % (self.id, self.first_date, self.last_date)        

class DiggestNews(models.Model):
    title = models.CharField(max_length=150)
    href = models.CharField(max_length=500)
    lang = models.CharField(max_length=2, choices=CHOISE_LANG, default='EN')
    category = models.ForeignKey(DiggetCategoryNews)
    desc = models.CharField(max_length=1000, blank=True, null=True)
    add_date = models.DateField(auto_now_add=True)
    diggest = models.ForeignKey(DiggestInfo)
    
    def __unicode__(self):
        return self.title