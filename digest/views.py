﻿from django.shortcuts import redirect, render_to_response, get_object_or_404, HttpResponse
from django.contrib.auth.decorators import login_required, permission_required
from django import http
from django.template import RequestContext
from digest.models import *
from digest.forms import *
import json

def get_active_diggest():
    from datetime import date, timedelta
    
    today = date.today()
    last_diggest = DiggestInfo.objects.get(archive=False)
    
    if today not in last_diggest.get_diggest_period():                                
              
        last_diggest.archive = True
        last_diggest.save()
        
        first_d = last_diggest.last_date + timedelta(days=1)
        last_d = last_diggest.last_date + timedelta(days=14)
        new_diggest = DiggestInfo(first_date=first_d, last_date=last_d)                                
        new_diggest.save()
        last_diggest = new_diggest
        
    return last_diggest


def manage_diggests(request):
    add_news_form = AddNewsForm()
    old_diggets = DiggestInfo.objects.filter(archive=True)
    return render_to_response('diggest-main.html',{'add_form':add_news_form, 'diggets_archive':old_diggets},context_instance=RequestContext(request))

@login_required
@permission_required('digest.add_diggestnews')     
def add_news_to_diggest(request):
    if request.method == 'POST':
        new_record = AddNewsForm(request.POST)
        if new_record.is_valid():            
            news_record = new_record.save(commit = False)                            
            news_record.diggest = get_active_diggest()                        
            
            try:
                DiggestNews.objects.get(href = news_record.href)
                request_json = {'error': [{'id':'href', 'error': 'Данная ссылка уже была ранее'}]}
            except DiggestNews.DoesNotExist:  
                request_json = {'error': None}
                news_record.save()
        else:
            request_json = {'error': [{'id': i, 'error': new_record[i].errors} for i in new_record.errors]}    
    
    return HttpResponse(json.dumps(request_json), content_type="application/json")

def view_old_diggest(request, diggest_id):
    diggest = get_object_or_404(DiggestInfo, pk=diggest_id)
    diggest_articles = DiggestNews.objects.filter(diggest=diggest).order_by('category')    
    return render_to_response('diggest-view.html',{'articles':diggest_articles, 'diggest_info':diggest},context_instance=RequestContext(request))