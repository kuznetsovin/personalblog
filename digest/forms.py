﻿from django import forms
from digest.models import DiggetCategoryNews, DiggestNews, CHOISE_LANG

class AddNewsForm(forms.ModelForm):
    title =  forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'Название')
    href = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u'Ссылка')
    lang = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=CHOISE_LANG, label=u'Язык')
    category = forms.ModelChoiceField(queryset=DiggetCategoryNews.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}), label=u'Категория')    
    desc = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control required'}), label=u'Описание',required=False)
    
    class Meta:
        model = DiggestNews
        fields = ['category','title','href','lang','desc']