﻿from django.conf.urls import patterns, include, url
from digest.views import *

urlpatterns = patterns('',
    url(r'^$', manage_diggests),
    url(r'^add_news$', add_news_to_diggest),
    url(r'^(?P<diggest_id>\d+)$', view_old_diggest),
)