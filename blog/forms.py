# -*- coding: utf-8 -*-
from django import forms
from blog.models import Post, Comments, Categores
from markitup.widgets import MarkItUpWidget

class NewPost(forms.ModelForm):
    text = forms.CharField(widget=MarkItUpWidget(), label=u"Текст статьи")
    #categories = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False, label=u"Категории")
    categories = forms.ModelMultipleChoiceField(queryset=Categores.objects.all().order_by('category'), widget = forms.SelectMultiple(attrs={'class': 'form-control'}))
    tags = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), label=u"Метки")
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), label=u"Заголовок")
    
    class Meta:
        model = Post
        fields = ('title', 'text', 'categories', 'tags')
  
class NewComment(forms.ModelForm):
    text = forms.CharField(widget=MarkItUpWidget(markitup_skin = '/markitup/skins/comment', markitup_set ='markitup/sets/markdown'), label=u"Комментарий")    
    
    class Meta:
        model = Comments
        fields = ('text',)