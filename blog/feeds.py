# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from blog.models import Post 

class RSSFeed(Feed): 
    title= u'Записки аналитика' 
    link= 'http://computerscinece.ru' 
    description= u'Последние опубликованные статьи c computerscinece.ru'
    
    def items(self): 
        return Post.objects.order_by('-date_post')[:5]

    def item_title(self, item):
        return item.title
    
    def item_description(self, item):
        val = unicode(item.text)    
        post_part = val.split('<a name="readmore"></a>')
        desc = post_part[0]
        if len(desc) > 1000 or len(post_part) == 2:    
            desc = desc[:1000]
            
        return desc
    
    def item_link(self, item):
        return reverse('posts', args=[item.pk])