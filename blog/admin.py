from django.contrib import admin
from blog.models import *

# Register your models here.
admin.site.register(Post)
admin.site.register(Tags)
admin.site.register(Categores)
admin.site.register(Comments)
admin.site.register(Images)
admin.site.register(Voiting)
