# -*- coding: utf8 -*-
from django import template

register = template.Library()

@register.filter
def readmore(value, idpost):
    val = unicode(value)    
    post_part = val.split('<a name="readmore"></a>')
    desc = post_part[0]
    href = u'<br/><a href="/posts/%s" class="btn btn-primary btn-sm">Читать далее</a>' % (str(idpost))
    if len(desc) > 1000 or len(post_part) > 1:    
        desc = desc[:1000] + href
    return desc
 
@register.filter 
def widget_category(value):
    ul_list = u"""
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Категории</h3>
              </div>
                <div class="panel-body">
                <ul>"""
    for category_name in value:
        ul_list += u'<li><a href="/categories/%s">%s</a></li>' % (category_name.id, category_name)
    ul_list += u"""</ul>
            </div>				
		</div>"""
    
    return ul_list