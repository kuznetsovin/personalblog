# -*- coding: utf8 -*-
from django.shortcuts import render, redirect, render_to_response, get_object_or_404, HttpResponse
from django.views.decorators.csrf import csrf_exempt 
from django.db.models import Count, Sum
from django import http
from django.template import RequestContext
from blog.models import *
from users.models import Profile
from computerscinece.settings import COUNT_POST
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from blog.forms import NewPost, NewComment
import json, re, ast
from functions import * 
from PIL import Image
     
def index(request, category_id=None, tag_id=None):
    if category_id is None and tag_id is None:
        posts = Post.objects.filter(is_draft=False)\
            .annotate(count_comments=Count('comments__id'))\
            .order_by('date_post').reverse()
    elif category_id is not None:
        posts = Post.objects.filter(categories__id=category_id)\
            .annotate(count_comments=Count('comments__id'))\
            .order_by('date_post').reverse()
    elif tag_id is not None:
        posts = Post.objects.filter(tags__id=tag_id)\
            .annotate(count_comments=Count('comments__id'))\
            .order_by('date_post').reverse()
        
    pages = Paginator(posts, COUNT_POST)
    
    page = request.GET.get('page')
    
    try:
        posts_pages = pages.page(page)
    except PageNotAnInteger:       
        posts_pages = pages.page(1)
    except EmptyPage:
        posts_pages = pages.page(pages.num_pages)

    return render_to_response('index.html', 
                              {'posts': posts_pages, 'cats': Categores.objects.all()},                              
                              context_instance=RequestContext(request))

def add_post(request):
    if request.method == 'POST':
        form = NewPost(request.POST) 
        if form.is_valid(): 
            post_title = form.cleaned_data['title']
            post_text_html = form.cleaned_data['text'] #(```) code in markdown
            author = Profile.objects.get(user = request.user.id)
            post = Post(author = author, title = post_title, text = post_text_html)
            post.save()
            
            for category in form.cleaned_data['categories']:               
                post.categories.add(category)
                
            for new_tag in form.cleaned_data['tags'].split(','):                    
                post.tags.add(token_validate(new_tag, Tags))
                
            return redirect('posts/%s' % post.id) 
    else:
        form = NewPost() 

    return render(request, 'add_post.html', {
        'form': form,
    })

def search_tags(request):
    return http.HttpResponse(token_search(request, Tags), content_type="application/json")

def post(request, id_post):
    article = get_object_or_404(Post, id=id_post)
    if article.rating > 0:
        color = 'positive'
    elif article.rating < 0:
        color = 'negative'
    else:
        color = ''
        
    if request.user.is_authenticated():
        favorites = Profile.objects.get(user_id = request.user.id).favorit_post
        if favorites is None:
            fav = 'favorite_add'
        elif str(article.id) in favorites:
            fav = 'favorite_add'
        else:
            fav = 'favorite_del'
    else:
        fav = 'favorite_add'
    
    soc = ast.literal_eval(article.author.social)
    
    try:
        google_author = '<a href="https://plus.google.com/%s?rel=author" class="googleplus_profile">G+</a>' % soc['gplus']
    except KeyError:
        google_author = ''
    
    comments = Comments.objects.filter(post_id=id_post).\
        extra(select={'color': "case when rating > 0 then 'positive' when rating < 0 then 'negative' else '' end"})
    url = request.build_absolute_uri()
    form_comment = NewComment()
    return render_to_response('post.html',
                              {
                              'post': article, 
                              'comments': comments, 
                              'post_url': url, 
                              'color': color, 
                              'fav':fav, 
                              'form_comment':form_comment, 
                              'g_plus_author':google_author
                              },                        
                              context_instance=RequestContext(request))
  
def add_comment(request):
    print request.POST;
    comment = Comments(
        user_id = request.user.id,
        text=request.POST['comment'],
        post_id=request.POST['post'],
		)
    if 'parentid' in request.POST and request.POST['parentid'] != '':
        print 'ok'
        comment.parent = Comments.objects.get(id=request.POST['parentid'])
    print comment
    comment.save()
    comments = Comments.objects.filter(post = comment.post_id)
    comment_tree = [{'id': c.id,'text': c.text.rendered,'level': c.level, 'rating': c.rating, 'username': c.user.user.username} for c in comments]   
    
    #return redirect('posts/%s' % request.POST['post_id'])
    return HttpResponse(json.dumps(comment_tree), content_type="application/json")

@csrf_exempt
def voiting(request):
    if request.is_ajax():
        if request.POST['post'] != '':
            id_post = request.POST['post']
            rec = Post.objects.get(id=id_post)
            is_post = True
        elif request.POST['comment'] != '':
            id_post = request.POST['comment']
            rec = Comments.objects.get(id=id_post)
            is_post = False
        
        try:
            if is_post:
                voicing_rec = Voiting.objects.get(user_id=request.user.id, post_id=id_post)
            else:
                voicing_rec = Voiting.objects.get(user_id=request.user.id, comment_id=id_post)               
            reply_voicing = True
        except Voiting.DoesNotExist:
                reply_voicing = False
                    
        if not reply_voicing: 
            user_voice = int(request.POST['voice'])        
            rec.rating += user_voice
            rec.save()
            usr = Profile.objects.get(user = request.user.id)
            if is_post:
                voicing_rec = Voiting(user=usr, post_id=id_post, voice=user_voice)
            else:
                voicing_rec = Voiting(user=usr, comment_id=id_post, voice=user_voice)
            voicing_rec.save()
            
        rating_now = {'new_rating': rec.rating, 'reply_voiting': reply_voicing}
        return HttpResponse(json.dumps(rating_now))

@csrf_exempt    
def favorites_post(request):
    if request.is_ajax():
        post = request.POST.get('post', False)
        usr_profile = Profile.objects.get(user_id = request.user.id)
        favorites = usr_profile.favorit_post
        o = {}
        
        if favorites is None: 
            fav = []
        else:
            fav = favorites.split(',')            
        
        if post not in fav:
            fav.append(post)
            o['operation'] = 1
        else:
            del fav[fav.index(post)]
            o['operation'] = 0
            
        usr_profile.favorit_post = ','.join(fav) #','.join(favorites)
        usr_profile.save()
        
        return HttpResponse(json.dumps(o))
        
@csrf_exempt
def image_upload(request):
    response = {'files': []}
    for pic in request.FILES.getlist('files[]'):
        img = Images(name=pic.name, image = pic)
        img.save()       
        response['files'].append({
            'name': '%s' % pic.name,
            'size': '%d' % pic.size,
            'url': '%s' % img.image.url,
            'thumbnailUrl': '%s' % img.image.path,
            'deleteUrl': '%s' % img.thumbnail.path,
            "deleteType": 'DELETE'
        })
 
    return HttpResponse(json.dumps(response), content_type='application/json')
    
def helplink_page(request):
    links = LinkHelp.objects.filter(is_dataset_link=False)
    return render_to_response('subpage.html',
                              {'links': links},                              
                              context_instance=RequestContext(request))
                              
def dataset_page(request):
    links = LinkHelp.objects.filter(is_dataset_link=True)
    return render_to_response('subpage.html',
                              {'links': links},                              
                              context_instance=RequestContext(request))
                              
def howto_page(request):
    howto_links = Post.objects.filter(is_draft=False, is_howto=True)
    return render_to_response('howto.html',
                              {'links': howto_links},                              
                              context_instance=RequestContext(request))                             