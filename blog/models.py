from django.db import models
from users.models import Profile
from mptt.models import MPTTModel, TreeForeignKey
from markitup.fields import MarkupField
# Create your models here.

class Tags(models.Model):
    name = models.TextField(max_length=512)

    def __unicode__(self):
        return self.name


class Categores(models.Model):
    category = models.TextField()

    def __unicode__(self):
        return self.category
        

class Post(models.Model):
    author = models.ForeignKey(Profile)
    text = MarkupField()
    date_post = models.DateField(auto_now_add=True)
    rating = models.IntegerField(max_length=5, default=0)  
    is_draft = models.BooleanField(default=True)
    is_howto = models.BooleanField(default=False)
    title = models.CharField(max_length=60)
    categories = models.ManyToManyField(Categores)
    tags = models.ManyToManyField(Tags)    
    
    def __unicode__(self):
        return self.title

class Comments(MPTTModel):
    post = models.ForeignKey(Post)
    user = models.ForeignKey(Profile)
    text = MarkupField()
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    rating = models.IntegerField(max_length=5, default=0)
    date_comment = models.DateField(auto_now_add=True)
    
    class MPTTMeta:
            # comments on one level will be ordered by date of creation
            order_insertion_by=['date_comment']

    def __unicode__(self):
        return self.text.raw
            
class Images(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='picture/%Y/%m')
    thumbnail = models.ImageField(upload_to='picture/%Y/%m', editable=False, null=True, blank=True)
    
    def create_thumbnail(self):
        if not self.image:
            return
 
        from PIL import Image
        from cStringIO import StringIO
        from django.core.files.uploadedfile import SimpleUploadedFile
        import os
 
        THUMBNAIL_SIZE = (100,100)
        
        
        NEWWIDTH = 600     

        DJANGO_TYPE = self.image.file.content_type
 
        if DJANGO_TYPE == 'image/jpeg':
             PIL_TYPE = 'jpeg'
             FILE_EXTENSION = 'jpg'
        elif DJANGO_TYPE == 'image/png':
             PIL_TYPE = 'png'
             FILE_EXTENSION = 'png'
 
        image = Image.open(StringIO(self.image.read()))
        name_f = os.path.split(self.image.name)[-1]
        
        if image.size[0] > NEWWIDTH:
            from django.conf import settings
            
            width_percent = (NEWWIDTH/float(image.size[0]))
            newheigth = int((float(image.size[1])*float(width_percent)))
        
            resize_image = image.resize((NEWWIDTH,newheigth),Image.ANTIALIAS)
            
            temp_handle = StringIO()
            resize_image.save(temp_handle, PIL_TYPE)
            temp_handle.seek(0)
        
            suf_resize_image = SimpleUploadedFile(name_f, temp_handle.read(), content_type=DJANGO_TYPE)
            self.image.save(name_f, suf_resize_image, save=False)
            
        image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
            
        temp_handle = StringIO()
        image.save(temp_handle, PIL_TYPE)
        temp_handle.seek(0)
 
        suf = SimpleUploadedFile(name_f, temp_handle.read(), content_type=DJANGO_TYPE)
        
        self.thumbnail.save('%s_thumbnail.%s'%(os.path.splitext(suf.name)[0],FILE_EXTENSION), suf, save=False)
                
    def save(self):
        self.create_thumbnail()
        super(Images, self).save()
 

class Voiting(models.Model):    
    user = models.ForeignKey(Profile)
    post = models.ForeignKey(Post, null=True) 
    comment = models.ForeignKey(Comments, null=True)
    voice = models.IntegerField(max_length=2)

    
class LinkHelp(models.Model):
    link = models.CharField(max_length=200)
    title = models.CharField(max_length=100)
    is_dataset_link = models.BooleanField(default=False)
    is_python_notebook = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.title
        